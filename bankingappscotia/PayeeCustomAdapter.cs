﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System.Collections.Generic;
using Android.Views;
using Android;

namespace bankingappscotia
{
    class PayeeCustomAdapter : BaseAdapter<InteracObject>
    {
        List<InteracObject> userList;
        Activity mycontext;

        public PayeeCustomAdapter(Activity contex, List<InteracObject> userArray)
        {
            userList = userArray;
            mycontext = contex;
        }

        public override InteracObject this[int position]
        {
            get { return userList[position]; }
        }

        public override int Count
        {
            get
            {
                return userList.Count;
            }
        }


        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View myView = convertView;
            InteracObject myObj = userList[position];

            if (myView == null)
            {
                myView = mycontext.LayoutInflater.Inflate(Resource.Layout.PayeeListCellLayout, null);
            }

            myView.FindViewById<TextView>(Resource.Id.nameID).Text = myObj.GetName();
            myView.FindViewById<TextView>(Resource.Id.emailID).Text = myObj.GetEmail();
            myView.FindViewById<TextView>(Resource.Id.secID).Text= myObj.GetSecValue();
            return myView;
        }
    }
}