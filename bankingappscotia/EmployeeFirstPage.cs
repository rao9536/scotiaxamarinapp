﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "EmployeeFirstPage")]
    public class EmployeeFirstPage : Activity
    {

        ListView myList;
        SearchView mySearch;
        // List<string> myArray = new List<string>();
        ArrayAdapter myAdapter;
        DBHelperClass myDB;
        IList customersList = new ArrayList();
        List<string> customerName = new List<string>();
        List<UserObject> list = new List<UserObject>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            myDB = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EmployeeFirstPage);
            System.Console.WriteLine("1111111111111111111111111111111111111111111111111111111111111 " + GlobalVariable.GetEmpId());
            // Create your application here

            myList = FindViewById<ListView>(Resource.Id.CustomersList);

            customersList = myDB.geCustomersList();

            for (int i = 0; i <= customersList.Count - 1; i++)
            {
                UserObject userObject = (UserObject)customersList[i];
                customerName.Add(userObject.GetName());

                list.Add((UserObject)customersList[i]);
            }

            EmployeeFirstPageCustomAdapter myAdapter = new EmployeeFirstPageCustomAdapter(this, list);
            myList.Adapter = myAdapter;
            myList.ItemClick += MyList_ItemClick;

            // get resource for search

            mySearch = FindViewById<SearchView>(Resource.Id.searchId);
            mySearch.QueryTextChange += MySearch_QueryTextChange;
        }

        private void MySearch_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var mySearchValue = e.NewText;
            System.Console.WriteLine("My search value is " + mySearchValue);
            string[] myArray = customerName.ToArray();
            List<UserObject> result = new List<UserObject>();
            int position = Array.IndexOf(myArray, mySearchValue);
            System.Console.WriteLine("My search value position is " + position);
            if (position > -1)
            {
                string value = myArray[position];

                for (int i = 0; i <= customersList.Count - 1; i++)
                {
                    UserObject userObject = (UserObject)customersList[i];
                    if (value.Equals(userObject.GetName()))
                    {
                        result.Add(userObject);
                    }
                }

                EmployeeFirstPageCustomAdapter myAdapter = new EmployeeFirstPageCustomAdapter(this, result);

                myList.Adapter = myAdapter;
                //myList.ItemClick += MyList_ItemClick;
            }
            else
            {
                EmployeeFirstPageCustomAdapter myAdapter = new EmployeeFirstPageCustomAdapter(this, list);
                myList.Adapter = myAdapter;
            }
        }


        private void MyList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var index = e.Position;
            string name = list[index].ToString();
            string[] dbData = myDB.selectByName(name);

            Intent EmployeeClientInfo = new Intent(this, typeof(EmployeeClientInfo));

            EmployeeClientInfo.PutExtra("name", dbData[0]);
            EmployeeClientInfo.PutExtra("email", dbData[1]);
            EmployeeClientInfo.PutExtra("accountNumber", dbData[2]);
            EmployeeClientInfo.PutExtra("dob", dbData[3]);
            EmployeeClientInfo.PutExtra("balance", dbData[4]);

            StartActivity(EmployeeClientInfo);

        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}