﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    class InteracObject
    {
        public string name;
        public string email;
        //private static string account;
        public string secValue;
        public string creationDate;

        public void SetName(string str)
        {
            name = str;
        }

        public string GetName()
        {
            return name;
        }

        public void SetEmail(string emailId)
        {
            email = emailId;
        }

        public string GetEmail()
        {
            return email;
        }

        /*public static void SetAccount(string accountNumber)
        {
            account = accountNumber;
        }

        public static string GetAccount()
        {
            return account;
        }*/

        public void SetSecValue(string securityValue)
        {
            secValue = securityValue;
        }

        public string GetSecValue()
        {
            return secValue;
        }

        public void SetCreationDate(string cDate)
        {
            creationDate = cDate;
        }

        public string GetCreationDate()
        {
            return creationDate;
        }
    }
}