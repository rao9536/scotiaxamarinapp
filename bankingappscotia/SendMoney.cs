﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "SendMoney")]
    public class SendMoney : Activity
    {
        Spinner spinnerView;
        DBHelperClass db;
        IList payeeEmailList = new ArrayList();
        string dropDownSelectValue;
        Button sendMoneyBtn;
        EditText amount;
        EditText secQues;
        Android.App.AlertDialog.Builder alert;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SendMoney);
            // Create your application here
            alert = new Android.App.AlertDialog.Builder(this);
            spinnerView = FindViewById<Spinner>(Resource.Id.dropDown);
            amount = FindViewById<EditText>(Resource.Id.amount);
            secQues = FindViewById<EditText>(Resource.Id.SecQue);
            sendMoneyBtn = FindViewById<Button>(Resource.Id.sendMoneyBtn);

            IList payeeList = db.gePayeeList();

            for (int i = 0; i <= payeeList.Count - 1; i++)
            {
                InteracObject interacObject = (InteracObject)payeeList[i];
                payeeEmailList.Add(interacObject.GetEmail());
            }

            sendMoneyBtn.Click += delegate
            {

                int  interacAmount = int.Parse(amount.Text);
                string secQuestion = secQues.Text;
                string toEmail = dropDownSelectValue;
                string senderaccount = GlobalVariable.GetAccount();
                string secValueDb = "";
                for (int i = 0; i <= payeeList.Count - 1; i++)
                {
                    InteracObject interacObject = (InteracObject)payeeList[i];
                    if (interacObject.GetEmail().Equals(toEmail)) 
                    {
                        secValueDb = interacObject.GetSecValue();
                        break;
                    }
                }
                if (secQuestion.Equals(secValueDb))
                {
                    IList userObjReceiverList = db.getUserObjectByEmail(toEmail);
                    AccountObject moneyReceiver = (AccountObject)userObjReceiverList[0];

                    int receriverAccBalance = int.Parse(moneyReceiver.GetInitialAcBalance());

                    IList userObjSenderList = db.getUserObjectByAccount(senderaccount);
                    AccountObject moneySender = (AccountObject)userObjSenderList[0];

                    int senderAccBalance = int.Parse(moneySender.GetInitialAcBalance());

                    if (senderAccBalance< interacAmount)
                    {
                        alert.SetTitle("Alert");
                        alert.SetMessage("Your account balance is not sufficient for the transaction");
                        alert.SetPositiveButton("OK", alertOK);
                        Dialog myDialog = alert.Create();
                        myDialog.Show();
                    }
                    else
                    {

                        int senderNewBalance = senderAccBalance - interacAmount;

                        int receiverNewBalance = receriverAccBalance + interacAmount;

                        db.updateInteracAccount(moneySender.GetEmail(), senderNewBalance);
                        db.updateInteracAccount(moneyReceiver.GetEmail(), receiverNewBalance);

                        SendEmails sendEmail = new SendEmails();
                        sendEmail.updateCustomerAccountStatus(moneySender.GetEmail(), senderNewBalance,true);
                        sendEmail.updateCustomerAccountStatus(moneyReceiver.GetEmail(), receiverNewBalance, false);

                        alert.SetTitle("Alert");
                        alert.SetMessage("Money is sent successfully to " + toEmail + " Check Email for details");
                        alert.SetPositiveButton("OK", alertOK);
                        Dialog myDialog = alert.Create();
                        myDialog.Show();
                    }

                }
                else
                {
                    alert.SetTitle("Alert");
                    alert.SetMessage("Security Question Value is Wrong");
                    alert.SetPositiveButton("OK", alertOK);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }

                Intent eTransfer = new Intent(this, typeof(eTransfer));
                StartActivity(eTransfer);
            };


            spinnerView.Adapter = new ArrayAdapter
                (this, Android.Resource.Layout.SimpleListItem1, payeeEmailList);



            spinnerView.ItemSelected += MyItemSelectedMethod;
        }

        private void MyItemSelectedMethod(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var index = e.Position;
            dropDownSelectValue = payeeEmailList[index].ToString();

        }
        public void alertOK(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("OK Button Pressed");
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}
