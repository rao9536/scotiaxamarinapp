﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Database;
using Android.Database.Sqlite;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Sql;
using System.Collections;

namespace bankingappscotia
{
    public class DBHelperClass : SQLiteOpenHelper
    {
        Context myContext;
        public static string DBname = "ScotiaBanking";
        public static string userTableName = "UserData";
        public static string userAccountTable = "UserAccountData";
        public static string interacDateTable = "UserInteracData";


        SQLiteDatabase connectionObj;

        public static string createTable = "CREATE TABLE " + userTableName + " (account_number int," +
            "name varchar(50),dob datetime,emailId varchar(50),password varchar(20),contact_number REAL,house_number int," +
            "city varchar(20),security_val varchar(300),postal_code varchar(10)," +
            "date_created datetime, date_updated datetime,isEmployee varchar(10),employeeId varchar(10));";


        public static string createUserAccountTable = "CREATE TABLE " + userAccountTable + " (account_number int NOT NULL," +
            "emailId varchar(50),security_val varchar(300),date_created datetime,date_updated datetime,initialAcBalance int," +
            "openingAcBalance int,PRIMARY KEY(account_number));";


        public static string interacTable = "Create table " + interacDateTable + "(name varchar(50)," +
            "emailId varchar(50) NOT NULL,date_created datetime,security_val varchar(300),PRIMARY KEY(emailId))";

        public string dropTable = "drop table " + userTableName;

        public override void OnCreate(SQLiteDatabase db)
        {

            Console.WriteLine(createTable);
            Console.WriteLine(createUserAccountTable);
            Console.WriteLine(interacTable);
            db.ExecSQL(createTable);
            db.ExecSQL(createUserAccountTable);
            db.ExecSQL(interacTable);
            //db.ExecSQL("drop table UserData");
           // db.ExecSQL("drop table UserAccountData");
            //db.ExecSQL("drop table UserInteracData");
        }


        public void deleteInterac(string email)
        {
            string deleteInterac ="delete from "+ interacDateTable + " where emailId=?;";
            connectionObj.ExecSQL(deleteInterac, new Java.Lang.Object[] { email });
        }

        public void insertSignUpData(string name_val, string dob_val, string email_val, string password_val, double contact_val,
            int houseNo_val, string city_val, string postal_val, string question_val, int accountNumber_val, string date_created, string isEmployee)
        {

            string insertData = "INSERT INTO " + userTableName + " (name, dob, emailId, password,contact_number,house_number,city," +
                "postal_code,security_val,account_number,date_created,isEmployee,employeeId) " +
                "VALUES('" + name_val + "','" + dob_val + "','" + email_val + "','" + password_val + "'," + contact_val + "," + houseNo_val + "," +
                "'" + city_val + "','" + postal_val + "','" + question_val + "'," + accountNumber_val + ",'" + date_created + "','" + isEmployee + "',null); ";

            Console.WriteLine("My SQL  Insert STM \n  \n" + insertData);

            connectionObj.ExecSQL(insertData);

        }


        public void insertEmployeeSignUpData(string name_val, string dob_val, string email_val, string password_val, string contact_val, string address_val, 
            string city_val, string postal_val, string question1_val, int employeeId, string date_created, string isEmployee)
        {
            string insertEmployeeSignUpData = "INSERT INTO " + userTableName + " (name, dob, emailId, password,contact_number,house_number,city," +
                "postal_code,security_val,employeeId,date_created,isEmployee,account_number) " +
                "VALUES('" + name_val + "','" + dob_val + "','" + email_val + "','" + password_val + "'," + contact_val + "," + address_val + "," +
                "'" + city_val + "','" + postal_val + "','" + question1_val + "'," + employeeId + ",'" + date_created + "','" + isEmployee + "', null); ";

            Console.WriteLine("My SQL  Insert STM \n  \n" + insertEmployeeSignUpData);
            connectionObj.ExecSQL(insertEmployeeSignUpData);
        }

       

        public string[] selectByName(string uname)
        {
            string[] dataArray = new string[5];
            ICursor cursor = null;
            string selectQuery = "select name,dob,emailId,account_number from" + userTableName + " where name=? ;";

            string accQuery = "select initialAcBalance from " + userAccountTable + "where account_number=?";

            try
            {
                string accNumber="";
                InteracObject interacObject = null;
                 cursor = connectionObj.RawQuery(selectQuery, new string[] { uname });
                while (cursor.MoveToNext())
                {
                    interacObject = new InteracObject();
                    string name = cursor.GetString(cursor.GetColumnIndexOrThrow("name"));
                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                     accNumber = cursor.GetString(cursor.GetColumnIndexOrThrow("account_number"));
                    string dob = cursor.GetString(cursor.GetColumnIndexOrThrow("dob"));
                    dataArray[0] = name;
                    dataArray[1] = emailId;
                    dataArray[2] = accNumber;
                    dataArray[3] = dob;

                }
                cursor = connectionObj.RawQuery(accQuery, new string[] { accNumber });
                while (cursor.MoveToNext())
                {
                    interacObject = new InteracObject();

                    string balance = cursor.GetString(cursor.GetColumnIndexOrThrow("initialAcBalance"));
                    dataArray[4] = balance;

                }

            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return dataArray;
        }

        public void updateInteracAccount(string email, int senderNewBalance)
        {
            string updateAccountQuery = "update " + userAccountTable + " SET initialAcBalance='" + senderNewBalance + "'  where emailId = ?; ";
            connectionObj.ExecSQL(updateAccountQuery, new Java.Lang.Object[] { email });
        }

        public IList getUserObjectByEmail(string toEmail)
        {
            IList accObjectList = new ArrayList();
            AccountObject accObject = new AccountObject();
            string userObjQuery = "select account_number,emailId,initialAcBalance from " + userAccountTable + " where emailId=?;";

            try
            {
                ICursor cursor = connectionObj.RawQuery(userObjQuery, new string[] { toEmail });
                while (cursor.MoveToNext())
                {
                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                    string accNumber = cursor.GetString(cursor.GetColumnIndexOrThrow("account_number"));
                    string initialAcBalance = cursor.GetString(cursor.GetColumnIndexOrThrow("initialAcBalance"));
                    accObject.SetEmail(emailId);
                    accObject.SetAccountNumber(accNumber);
                    accObject.SetInitialAcBalance(initialAcBalance);
                }
                accObjectList.Add(accObject);
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }

            return accObjectList;

        }



        public IList getUserObjectByAccount(string account)
        {
            IList accObjectList = new ArrayList();
            AccountObject accObject = new AccountObject();
            string userObjQuery = "select account_number,emailId,initialAcBalance from " + userAccountTable + " where account_number=?;";

            try
            {
                ICursor cursor = connectionObj.RawQuery(userObjQuery, new string[] { account });
                while (cursor.MoveToNext())
                {
                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                    string accNumber = cursor.GetString(cursor.GetColumnIndexOrThrow("account_number"));
                    string initialAcBalance = cursor.GetString(cursor.GetColumnIndexOrThrow("initialAcBalance"));
                    accObject.SetEmail(emailId);
                    accObject.SetAccountNumber(accNumber);
                    accObject.SetInitialAcBalance(initialAcBalance);

                }
                accObjectList.Add(accObject);
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }

            return accObjectList;
        }


        public void saveInterac(string name, string email, string securitQuesValue,string createDate)
        {

            string saveInteracQuery = "INSERT INTO " + interacDateTable + " (name, emailId,security_val,date_created) " +
                "VALUES('" + name + "','" + email + "','" + securitQuesValue + "','"+ createDate + "'); ";

            Console.WriteLine("My SQL  Insert STM \n  \n" + saveInteracQuery);

            connectionObj.ExecSQL(saveInteracQuery);
        }


        public IList gePayeeList()
        {

            IList payeeObjectList = new ArrayList();
            string payeeQuery = "select * from " + interacDateTable + ";";

            try
            {
                
                InteracObject interacObject = null;
                ICursor cursor = connectionObj.RawQuery(payeeQuery, new string[] { });
                while (cursor.MoveToNext())
                {
                    interacObject = new InteracObject();

                    string name = cursor.GetString(cursor.GetColumnIndexOrThrow("name"));
                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                    string creationDate = cursor.GetString(cursor.GetColumnIndexOrThrow("date_created"));
                    string securityVal = cursor.GetString(cursor.GetColumnIndexOrThrow("security_val"));

                    interacObject.SetName(name);
                    interacObject.SetEmail(emailId);
                    interacObject.SetSecValue(securityVal);
                    interacObject.SetCreationDate(creationDate);
                    payeeObjectList.Add(interacObject);
                }
                

            }
            catch (Exception e)
            {
                e.GetBaseException();
            }


            return payeeObjectList;
        }


        public IList geCustomersList()
        {
            IList UserObjectList = new ArrayList();
            string payeeQuery = "select name, account_number from " + userTableName + ";";

            try
            {

                UserObject userObject = null;
                ICursor cursor = connectionObj.RawQuery(payeeQuery, new string[] { });
                while (cursor.MoveToNext())
                {
                    userObject = new UserObject();

                    string name = cursor.GetString(cursor.GetColumnIndexOrThrow("name"));
                    string accountNumber = cursor.GetString(cursor.GetColumnIndexOrThrow("account_number"));

                    userObject.SetName(name);
                    userObject.SetAccountNumber(accountNumber);

                    UserObjectList.Add(userObject);
                }

            }
            catch (Exception e)
            {
                e.GetBaseException();
            }

            return UserObjectList;
        }




        public void updateUserDetailsValue(string name, string contact, string dob, string houseNo, string city, string postal, string Question1,
            string accountNumber)
        {
            string updateQuery = "update " + userTableName + " SET name='"+name+ "', contact_number='"+ contact+ "',dob='"+ dob+ 
                "',house_number='"+ houseNo+ "',city='"+ city+ "',postal_code='"+ postal+ "',security_val='"+ Question1+ "'  where account_number = ?; ";
            Console.WriteLine(updateQuery);
            connectionObj.ExecSQL(updateQuery, new Java.Lang.Object[] {accountNumber});
        }




        public IList GetUserDetails(string accountNumber)
        {
            IList userObjectList = new ArrayList();
            UserObject userObject = new UserObject();

            string query = "select * from " + userTableName + " where account_number= ?;";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { accountNumber });
                while (cursor.MoveToNext())
                {
                    string name = cursor.GetString(cursor.GetColumnIndexOrThrow("name"));
                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                    string accountNum = cursor.GetString(cursor.GetColumnIndexOrThrow("account_number"));
                    string dob = cursor.GetString(cursor.GetColumnIndexOrThrow("dob"));
                    string contact = cursor.GetString(cursor.GetColumnIndexOrThrow("contact_number"));
                    string houseNo = cursor.GetString(cursor.GetColumnIndexOrThrow("house_number"));
                    string city = cursor.GetString(cursor.GetColumnIndexOrThrow("city"));
                    string securityVal = cursor.GetString(cursor.GetColumnIndexOrThrow("security_val"));
                    string postalCode = cursor.GetString(cursor.GetColumnIndexOrThrow("postal_code"));

                    userObject.SetName(name);
                    userObject.SetEmail(emailId);
                    userObject.SetAccountNumber(accountNum);
                    userObject.SetDOB(dob);
                    userObject.SetContactNumber(contact);
                    userObject.SetHouseNumber(houseNo);
                    userObject.SetCity(city);
                    userObject.SetSeccurityValue(securityVal);
                    userObject.SetPostalCode(postalCode);

                }
                userObjectList.Add(userObject);

            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return userObjectList;

        }

        public void updatePassword(string pwd, string accountNum)
        {
            string updatePwdQuery = "update " + userTableName + " SET password='" + pwd + "'  where account_number = ?; ";
            Console.WriteLine(updatePwdQuery);
            connectionObj.ExecSQL(updatePwdQuery, new Java.Lang.Object[] { accountNum });
        }

        public void updateInteracPayeeDetails(string email, string name, string secQue)
        {
            string updateInteracPayeeQuery = "update " + interacDateTable + " SET name='" + name + "',security_val='" + secQue + "'  where emailId = ?; ";
            connectionObj.ExecSQL(updateInteracPayeeQuery, new Java.Lang.Object[] { email });
        }

        public string[] getInteracObj(string email)
        {
            string[] interacObj = new string[3];

            string query = "select name,security_val,emailId from " + interacDateTable + " where emailId= ?;";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { email });
                while (cursor.MoveToNext())
                {
                    string name = cursor.GetString(cursor.GetColumnIndexOrThrow("name"));
                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                    string secValue = cursor.GetString(cursor.GetColumnIndexOrThrow("security_val"));
                    interacObj[0] = name;
                    interacObj[1] = emailId;
                    interacObj[2] = secValue;
                }

            }
            catch (Exception e)
            {
                e.GetBaseException();
            }


            return interacObj;
        }

        

        public DBHelperClass(Context context) : base(context, name: DBname, factory: null, version: 1)
        {
            myContext = context;
            connectionObj = WritableDatabase;

        }


        public IList geUserEmailIds()
        {
            IList emailIdsList = new ArrayList();
            string query = "select  emailId from " + userTableName + ";";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { });
                while (cursor.MoveToNext())
                {

                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));

                    emailIdsList.Add(emailId);

                }
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return emailIdsList;
        }

        public IList geUserContacts()
        {
            IList contactsList = new ArrayList();
            string query = "select  contact_number from " + userTableName + ";";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { });
                while (cursor.MoveToNext())
                {

                    int contactNumber = cursor.GetInt(cursor.GetColumnIndexOrThrow("contact_number"));

                    contactsList.Add(contactNumber);

                }
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return contactsList;
        }



        public IList geUseraccountNos()
        {
            IList accountNosList = new ArrayList();
            string query = "select  account_number from " + userTableName + ";";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { });
                while (cursor.MoveToNext())
                {

                    int accountNumber = cursor.GetInt(cursor.GetColumnIndexOrThrow("account_number"));

                    accountNosList.Add(accountNumber);

                }
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return accountNosList;
        }

        public IList geEmployeeIds()
        {
            IList EmployeeIdsList = new ArrayList();
            string query = "select  employeeId from " + userTableName + ";";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { });
                while (cursor.MoveToNext())
                {

                    int EmployeeIds = cursor.GetInt(cursor.GetColumnIndexOrThrow("employeeId"));

                    EmployeeIdsList.Add(EmployeeIds);

                }
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return EmployeeIdsList;
        }


        public string[] validateUserLogin(double accNum)
        {
            string acc = accNum.ToString();
            string[] dataArray = new string[2];

            string accQuery = "select password,isEmployee from " + userTableName + " where account_number= ? OR employeeId= ?;";

            try
            {
                ICursor cursor = connectionObj.RawQuery(accQuery, new string[] {acc});
                while (cursor.MoveToNext())
                {
                    string password = cursor.GetString(cursor.GetColumnIndexOrThrow("password"));
                    string isEmployee = cursor.GetString(cursor.GetColumnIndexOrThrow("isEmployee"));
                    dataArray[0] = password;
                    dataArray[1] = isEmployee;
                }
                if (cursor.Count <= 0)
                {
                    string empIdQuery = "select password,isEmployee from " + userTableName + " where employeeId= ?;";
                    cursor = connectionObj.RawQuery(empIdQuery, new string[] { acc });

                    while (cursor.MoveToNext())
                    {
                        string password = cursor.GetString(cursor.GetColumnIndexOrThrow("password"));
                        string isEmployee = cursor.GetString(cursor.GetColumnIndexOrThrow("isEmployee"));
                        dataArray[0] = password;
                        dataArray[1] = isEmployee;
                    }

                  
                }
                {

                }

            }
            catch (Exception e)
            {
                e.GetBaseException();
            }
            return dataArray;

        }

        public IList geUserData()
        {
            IList dataList = new ArrayList();

            string query = "select  emailId,contact_number,account_number from " + userTableName + ";";

            try
            {
                ICursor cursor = connectionObj.RawQuery(query, new string[] { });
                while (cursor.MoveToNext())
                {
                    string[] dataArray = new string[2];


                    string emailId = cursor.GetString(cursor.GetColumnIndexOrThrow("emailId"));
                    int contactNumber = cursor.GetInt(cursor.GetColumnIndexOrThrow("contact_number"));
                    int accountNumber = cursor.GetInt(cursor.GetColumnIndexOrThrow("account_number"));

                    dataArray[0] = emailId;
                    dataArray[1] = contactNumber.ToString();
                    dataArray[2] = contactNumber.ToString();

                    dataList.Add(dataArray);

                }
            }
            catch (Exception e)
            {
                e.GetBaseException();
            }


            return dataList;
        }

        public void insertAccountData(string email_val, string question_val, int accountNumber, string date_created, int initial_Balance, int minimum_Balance)
        {
            string insertAccountDataValues = "INSERT INTO " + userAccountTable + " (emailId, security_val, account_number, date_created, initialAcBalance,openingAcBalance) " +
                "VALUES('" + email_val + "','" + question_val + "'," + accountNumber + ",'" + date_created + "'," + initial_Balance + "," + minimum_Balance + "); ";


        Console.WriteLine("My SQL  Insert STM \n  \n" + insertAccountDataValues);

            connectionObj.ExecSQL(insertAccountDataValues);
        }



        public override void OnUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            throw new NotImplementedException();
        }

    }
}