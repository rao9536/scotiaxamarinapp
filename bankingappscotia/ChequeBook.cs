﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "ChequeBook")]
    public class ChequeBook : Activity
    {
        DBHelperClass db;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);

            // Create your application here

            string accountNum = GlobalVariable.GetAccount();

            IList userObjectList = db.GetUserDetails(accountNum);
            UserObject userObject = (UserObject)userObjectList[0];

            SendEmails sendEmail = new SendEmails();

            sendEmail.sendChequeBookEmail(userObject.GetName(), userObject.GetEmail(), userObject.GetHouseNumber(), userObject.GetCity(), userObject.GetPostalCode());

        Intent mainActivity = new Intent(this, typeof(Services));
            StartActivity(mainActivity);
        }
    }
}