﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using System;
using Android.Views;
using Android;

namespace bankingappscotia
{
    [Activity(Label = "@string/app_name",Icon ="@drawable/icon", MainLauncher = true)]
    public class MainActivity : Activity
    {   
        // declaring variables to login
        EditText AccountNum;
        EditText password;
        Button Loginbtn;
        Button SignUpBtn;
        EditText securityCode;

        //for creating alert
        Android.App.AlertDialog.Builder alert;

        // DBhelper class object
        DBHelperClass db;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            // set the values from layout by relating ids
            AccountNum = FindViewById<EditText>(Resource.Id.AccId);
            password = FindViewById<EditText>(Resource.Id.password);
            Loginbtn = FindViewById<Button>(Resource.Id.loginbtn);
            SignUpBtn = FindViewById<Button>(Resource.Id.signup);

            // creating alert if user does not enter the value
            alert = new Android.App.AlertDialog.Builder(this);

            // functionality of login button
            Loginbtn.Click += delegate
            {

                double AccountNum_val = Convert.ToDouble(AccountNum.Text);
                string pass_val = password.Text;
                if (AccountNum_val.Equals(""))
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter Account Number/Employee Id");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                else if( pass_val.Trim().Equals("") || pass_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter Your Password");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else
                {
                    string[] userData = db.validateUserLogin(AccountNum_val);

                    if (string.IsNullOrEmpty(userData[0]) || string.IsNullOrEmpty(userData[1]))
                    {
                        //alert for wrong username and password
                        alert.SetTitle("Error");
                        alert.SetMessage("Account/Employee ID or Password is incorrect");
                        alert.SetPositiveButton("OK", alertOK);
                        Dialog myDialog = alert.Create();
                        myDialog.Show();
                    }
                    else { 

                    if (userData[0].Equals(pass_val) && String.Equals(userData[1], (string)"False",
                   StringComparison.OrdinalIgnoreCase))
                    {
                        GlobalVariable.SetAccount(AccountNum_val.ToString());
                        GlobalVariable.SetPassword(pass_val.ToString());
                        // Go to the services provided for the user screen
                        Intent userServices = new Intent(this, typeof(Services));

                        StartActivity(userServices);
                    }
                    else if (userData[0].Equals(pass_val) && String.Equals(userData[1], (string)"true",
                   StringComparison.OrdinalIgnoreCase))
                    {
                        GlobalVariable.SetEmpId(AccountNum_val.ToString());
                        GlobalVariable.SetPassword(pass_val.ToString());
                        Intent empServices = new Intent(this, typeof(EmployeeLoginOption));

                        StartActivity(empServices);
                    }
                    }

                }


            };
            SignUpBtn.Click += delegate

            {
                // go to screen to take the details of the user
                /*  Intent Details = new Intent(this, typeof(UserDetail));
                  StartActivity(Details);*/
          
                alert.SetTitle("Are you a employee?");
                alert.SetMessage("Enter the security code?");
                alert.SetPositiveButton("Yes", alertYes);
                alert.SetNegativeButton("No", alertNo);
                securityCode = new EditText(this);
                alert.SetView(securityCode);
                Dialog myDialog = alert.Create();
                myDialog.Show();
               // return securityCode;

            };


        }
        public void alertOK(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("OK Button Pressed");
        }
        public void alertcancel(object sender, Android.Content.DialogClickEventArgs e)
        {

            Console.WriteLine("Cancel Button Pressed");
        }
        public void alertYes(object sender, Android.Content.DialogClickEventArgs e)
        {
            Console.WriteLine("Yes Button Pressed");
            alert = new Android.App.AlertDialog.Builder(this);
            if ((securityCode.Text).Equals("12345"))
            {
                Intent Details = new Intent(this, typeof(UserDetail));
                StartActivity(Details);

            }
            else
            {
                alert.SetTitle("Error");
                alert.SetPositiveButton("OK", alertOK);
                alert.SetNegativeButton("Cancel", alertcancel);
                Dialog error = alert.Create();
                error.Show();
            }
        }
        public void alertNo(object sender, Android.Content.DialogClickEventArgs e)
        {

            Console.WriteLine("No Button Pressed");
        }

       
    }
}
    