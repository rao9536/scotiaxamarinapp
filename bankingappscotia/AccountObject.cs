﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    class AccountObject
    {
        public string accountNumber;
        public string email;
        public string initialAcBalance;

        public void SetAccountNumber(string acc)
        {
            accountNumber = acc;
        }

        public string GetAccountNumber()
        {
            return accountNumber;
        }

        public void SetEmail(string str)
        {
            email = str;
        }

        public string GetEmail()
        {
            return email;
        }

        public void SetInitialAcBalance(string balance)
        {
            initialAcBalance = balance;
        }

        public string GetInitialAcBalance()
        {
            return initialAcBalance;
        }
    }
}