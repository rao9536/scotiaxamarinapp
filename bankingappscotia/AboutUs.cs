﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android;
using Android.App;
using Android.OS;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "AboutUs")]
    public class AboutUs : Activity
    {
        // declaring variables
        TextView line1Txt;
        TextView line2Txt;
        TextView line3Txt;
        TextView line4Txt;
        TextView line5Txt;
        TextView line6Txt;
        TextView line7Txt;
        TextView line8Txt;
        TextView line9Txt;
        TextView line10Txt;



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.About);
            // SetContentView(Resource.Layout.);


            // Create your application here


            line1Txt = FindViewById<TextView>(Resource.Id.line1);
            line2Txt = FindViewById<TextView>(Resource.Id.line2);
            line3Txt = FindViewById<TextView>(Resource.Id.line3);
            line4Txt = FindViewById<TextView>(Resource.Id.line4);
            line5Txt = FindViewById<TextView>(Resource.Id.line5);
            line6Txt = FindViewById<TextView>(Resource.Id.line6);
            line7Txt = FindViewById<TextView>(Resource.Id.line7);
            line8Txt = FindViewById<TextView>(Resource.Id.line8);
            line9Txt = FindViewById<TextView>(Resource.Id.line9);
            line10Txt = FindViewById<TextView>(Resource.Id.line10);


            line1Txt.Text = "Scotiabank is Canada's international bank and ";
            line2Txt.Text = "a leading financial services provider in the North America.";
            line3Txt.Text = "We are dedicated to helping";
            line4Txt.Text = " more than 25 million customers.";
            line5Txt.Text = "Recognizing special needs of ";
            line6Txt.Text = " Canadian farms and agri-businesses.";
            line7Txt.Text = "Specially designed products and services.";
            line8Txt.Text = "Commitment to our customers' futures, ";
            line9Txt.Text = "including succession planning for the next generation.";
            line10Txt.Text = "Highest degree of flexibility";
        }
    }
}
