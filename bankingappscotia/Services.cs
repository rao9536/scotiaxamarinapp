﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace bankingappscotia
{
    [Activity(Label = "Services")]
    public class Services : Activity
    {
        ListView myList;
        TextView printUserName;

        string[] serviceArray = { "My Profile", "Cheque Book Request", "e-Transfer", "Account Balance" };

        ArrayAdapter myAdapter;

        Android.App.AlertDialog.Builder alert;

        DBHelperClass myDB;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            myDB = new DBHelperClass(this);

            System.Console.WriteLine("1111111111111111111111111111111111111111111111111111111111111 " + GlobalVariable.GetAccount());
            alert = new Android.App.AlertDialog.Builder(this);

            // Create your application here
            SetContentView(Resource.Layout.Services);
            myList = FindViewById<ListView>(Resource.Id.myListView);
            printUserName = FindViewById<TextView>(Resource.Id.userName);
            printUserName.Text = "Welcome ";// add here the value to name of user from the database

            myAdapter = new ArrayAdapter
                (this, Android.Resource.Layout.SimpleListItem1, serviceArray);
            myList.Adapter = myAdapter;
            myList.ItemClick += myIteamClickMethod;


        }


        public void myIteamClickMethod(object sender, AdapterView.ItemClickEventArgs e)
        {
            //System.Console.WriteLine("I am clicking on the list item \n\n");
            var indexValue = e.Position;
            if (indexValue.Equals(0))
            {
                Intent Profile = new Intent(this, typeof(UserProfile));
                StartActivity(Profile);
            }
            else if (indexValue.Equals(1))
            {
                alert.SetTitle("Alert");
                alert.SetMessage("Your cheque book request is accepted. Please check your E-mail for more details");
                alert.SetPositiveButton("OK", alertOK);
                Dialog myDialog = alert.Create();
                myDialog.Show();

                Intent cheque = new Intent(this, typeof(ChequeBook));
                StartActivity(cheque);
            }
            else if (indexValue.Equals(2))
            {
                Intent eTransfer = new Intent(this, typeof(eTransfer));
                StartActivity(eTransfer);
            }
            else if (indexValue.Equals(3))
            {
                SendEmails sendEmails = new SendEmails();

                IList userObjList = myDB.getUserObjectByAccount(GlobalVariable.GetAccount());
                AccountObject accObj = (AccountObject)userObjList[0];

                sendEmails.balanceInquiry(accObj.GetEmail(), accObj.GetInitialAcBalance(), accObj.GetAccountNumber());

                Intent Services = new Intent(this, typeof(Services));
                StartActivity(Services);
            }

            var myValue = serviceArray[indexValue];
            //System.Console.WriteLine("Value is \n\n " + myValue);
        }

        private void alertOK(object sender, DialogClickEventArgs e)
        {
            System.Console.WriteLine("OK Button Pressed");
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        // add your code 
                        
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }

    }
}
      
   
   