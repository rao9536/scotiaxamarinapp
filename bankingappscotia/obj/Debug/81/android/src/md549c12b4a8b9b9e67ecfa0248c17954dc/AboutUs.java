package md549c12b4a8b9b9e67ecfa0248c17954dc;


public class AboutUs
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("bankingappscotia.AboutUs, bankingappscotia", AboutUs.class, __md_methods);
	}


	public AboutUs ()
	{
		super ();
		if (getClass () == AboutUs.class)
			mono.android.TypeManager.Activate ("bankingappscotia.AboutUs, bankingappscotia", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
