﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "eTransfer")]
    public class eTransfer : Activity
    {
        string[] serviceArray = { "Add New Payee", "Edit/Delete Payee", "Transfer Money","Payee List" };
        ListView myList;
        ArrayAdapter myAdapter;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.eTransfer);
            myList = FindViewById<ListView>(Resource.Id.myListView);

            myAdapter = new ArrayAdapter
                (this, Android.Resource.Layout.SimpleListItem1, serviceArray);
            myList.Adapter = myAdapter;
            myList.ItemClick += myIteamClickMethod;

        }
        public void myIteamClickMethod(object sender, AdapterView.ItemClickEventArgs e)
        {
            //System.Console.WriteLine("I am clicking on the list item \n\n");
            var indexValue = e.Position;
            if (indexValue.Equals(0))
            {
                Intent AddPayee = new Intent(this, typeof(AddPayee));
                StartActivity(AddPayee);
            }
            else if (indexValue.Equals(1))
            {
                Intent SelectPayee = new Intent(this, typeof(SelectPayee));
                StartActivity(SelectPayee);
            }
            else if (indexValue.Equals(2))
            {
                Intent SendMoney = new Intent(this, typeof(SendMoney));
                StartActivity(SendMoney);
            }
            else if (indexValue.Equals(3))
            {
                Intent ListPayee = new Intent(this, typeof(ListPayee));
                StartActivity(ListPayee);
            }

            var myValue = serviceArray[indexValue];
            //System.Console.WriteLine("Value is \n\n " + myValue);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}