﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "SelectPayee")]
    class SelectPayee : Activity
    {
        Spinner spinnerView;
        DBHelperClass db;
        IList payeeNameList = new ArrayList();
        string dropDownSelectValue;
        Button EditBtn;
        Button DeleteBtn;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SelectPayee);

            spinnerView = FindViewById<Spinner>(Resource.Id.dropDown);

            EditBtn = FindViewById<Button>(Resource.Id.EditBtn);
            DeleteBtn = FindViewById<Button>(Resource.Id.DeleteBtn);

            IList payeeList = db.gePayeeList();

            DeleteBtn.Click += delegate
            {
                db.deleteInterac(dropDownSelectValue);
                Intent eTransfer = new Intent(this, typeof(eTransfer));
                StartActivity(eTransfer);
            };

            EditBtn.Click += delegate
            {
                GlobalVariable.SetEditPayeeEmail(dropDownSelectValue);
                Intent EditPayee = new Intent(this, typeof(EditPayee));
                StartActivity(EditPayee);
            };



            for (int i = 0; i <= payeeList.Count - 1; i++)
            {
                InteracObject interacObject = (InteracObject)payeeList[i];
                payeeNameList.Add(interacObject.GetEmail());
            }

            spinnerView.Adapter = new ArrayAdapter
                (this, Android.Resource.Layout.SimpleListItem1, payeeNameList);



            spinnerView.ItemSelected += MyItemSelectedMethod;
        }

        private void MyItemSelectedMethod(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var index = e.Position;
            dropDownSelectValue = payeeNameList[index].ToString();

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }


    }
}