﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "UserProfile")]
    public class UserProfile : Activity
    {
        // declaring variables
        EditText name;
        EditText dob;
        EditText email;
        EditText accountNumber;
        EditText contact;
        EditText houseNo;
        EditText city;
        EditText postal;
        EditText Question1;

        Button editBtn;

        DBHelperClass myDB;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            myDB = new DBHelperClass(this);
            // Create your application here

            SetContentView(Resource.Layout.UserProfile);

            IList userObjectList = myDB.GetUserDetails(GlobalVariable.GetAccount().ToString());

            UserObject userObject = (UserObject)userObjectList[0];

            // variables belongs to xml file
            name = FindViewById<EditText>(Resource.Id.nameId);
            dob = FindViewById<EditText>(Resource.Id.dob);
            email = FindViewById<EditText>(Resource.Id.email);
            accountNumber = FindViewById<EditText>(Resource.Id.accountNumber);
            contact = FindViewById<EditText>(Resource.Id.Contact);
            houseNo = FindViewById<EditText>(Resource.Id.Address);
            city = FindViewById<EditText>(Resource.Id.city);
            postal = FindViewById<EditText>(Resource.Id.postalCode);
            Question1 = FindViewById<EditText>(Resource.Id.Question1);
            editBtn = FindViewById<Button>(Resource.Id.editBtn);

            // show the database data in field

            name.Text = userObject.GetName();
            email.Text = userObject.GetEmail();
            dob.Text = userObject.GetDOB();
            accountNumber.Text = userObject.GetAccountNumber();
            contact.Text = userObject.GetContactNumber();
            houseNo.Text = userObject.GetHouseNumber();
            city.Text = userObject.GetCity();
            postal.Text = userObject.GetPostalCode();
            Question1.Text = userObject.GetSeccurityValue();


            // disabling the enteries
            name.Enabled = false;
            dob.Enabled = false;
            email.Enabled = false;
            accountNumber.Enabled = false;
            contact.Enabled = false;
            houseNo.Enabled = false;
            city.Enabled = false;
            postal.Enabled = false;
            Question1.Enabled = false;


            editBtn.Click += Edit_Click;


        }



        // edit button method
        private void Edit_Click(object sender, EventArgs e)
        {
            name.Enabled = true;
            contact.Enabled = true;
            dob.Enabled = true;
            houseNo.Enabled = true;
            city.Enabled = true;
            postal.Enabled = true;
            Question1.Enabled = true;

            editBtn.Text = "Save";
            editBtn.Click += Save_Click;

        }

        private void Save_Click(object sender, EventArgs e)
        {
            name = FindViewById<EditText>(Resource.Id.nameId);
            contact = FindViewById<EditText>(Resource.Id.Contact);
            dob = FindViewById<EditText>(Resource.Id.dob);
            houseNo = FindViewById<EditText>(Resource.Id.Address);
            city = FindViewById<EditText>(Resource.Id.city);
            postal = FindViewById<EditText>(Resource.Id.postalCode);
            Question1 = FindViewById<EditText>(Resource.Id.Question1);
            accountNumber = FindViewById<EditText>(Resource.Id.accountNumber);

            myDB.updateUserDetailsValue(name.Text, contact.Text, dob.Text, houseNo.Text, city.Text, postal.Text, Question1.Text, accountNumber.Text);

            Intent login = new Intent(this, typeof(Services));
            StartActivity(login);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }


    }   
}