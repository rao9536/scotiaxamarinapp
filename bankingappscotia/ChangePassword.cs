﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{

    [Activity(Label = "ChangePassword")]
    public class ChangePassword : Activity
    {
        EditText currentPwd;
        EditText newPwd;
        EditText confirmPwd;
        Button saveBtn;
        Android.App.AlertDialog.Builder alert;
        DBHelperClass db;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.PasswordChange);
            // Create your application here

            currentPwd = FindViewById<EditText>(Resource.Id.CurrentPassword);
            newPwd = FindViewById<EditText>(Resource.Id.NewPassword);
            confirmPwd = FindViewById<EditText>(Resource.Id.ConfirmPassword);
            saveBtn = FindViewById<Button>(Resource.Id.saveBtn);

            currentPwd.Text = GlobalVariable.GetPassword();
            alert = new Android.App.AlertDialog.Builder(this);


            saveBtn.Click += delegate
            {
                string npwd = newPwd.Text;
                string copwd = confirmPwd.Text;
                if (!npwd.Trim().Equals(copwd.Trim()))
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("New password and confirm password must match");
                    alert.SetPositiveButton("OK", alertOK);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                saveBtn.Click += Submit_Click;
            };

        }

        private void alertOK(object sender, DialogClickEventArgs e)
        {
            System.Console.WriteLine("OK Button Pressed");
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            currentPwd = FindViewById<EditText>(Resource.Id.CurrentPassword);
            newPwd = FindViewById<EditText>(Resource.Id.NewPassword);
            confirmPwd = FindViewById<EditText>(Resource.Id.ConfirmPassword);

            string accountNum = GlobalVariable.GetAccount();

            db.updatePassword(newPwd.Text, accountNum);

            IList userObjectList = db.GetUserDetails(accountNum);
            UserObject userObject = (UserObject)userObjectList[0];

            SendEmails sendEmail = new SendEmails();
        

            sendEmail.sendChangePasswordEmail(newPwd.Text,currentPwd.Text, userObject.GetName(), userObject.GetEmail());

            Intent mainActivity = new Intent(this, typeof(MainActivity));
            StartActivity(mainActivity);
        }
    }
}