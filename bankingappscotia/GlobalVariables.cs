﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    public static class GlobalVariable
    {
        private static string account;
        private static string empId;
        private static string password;
        private static string editPayeeEmail;

        public static void SetAccount(string acc)
        {
            account = acc;
        }

        public static string GetAccount()
        {
            return account;
        }

        public static void SetPassword(string pwd)
        {
            password = pwd;
        }

        public static string GetPassword()
        {
            return password;
        }

        public static void SetEmpId(string employeeID)
        {
            empId = employeeID;
        }

        public static string GetEmpId()
        {
            return empId;
        }


        public static void SetEditPayeeEmail(string payeeEmail)
        {
            editPayeeEmail = payeeEmail;
        }

        public static string GetEditPayeeEmail()
        {
            return editPayeeEmail;
        }

    }
}