﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "EmployeeLoginOption")]
    class EmployeeLoginOption : Activity
    {
        TextView line1Txt;
        Button UserDetail;
        Button sign;
        Button deposit;
        Button withdraw;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.EmployeeLoginOption);
            line1Txt = FindViewById<TextView>(Resource.Id.line1);
            UserDetail = FindViewById<Button>(Resource.Id.userListbtn);
            sign = FindViewById<Button>(Resource.Id.signbtn);

            deposit = FindViewById<Button>(Resource.Id.depositbtn);
            withdraw = FindViewById<Button>(Resource.Id.withdrawbtn);

            line1Txt.Text = "Welcome to Scotia Bank Admin Panel !!";
            UserDetail.Click += delegate
            {

                Intent UserList = new Intent(this, typeof(EmployeeFirstPage));

                StartActivity(UserList);
            };
            sign.Click += delegate
            {
                Intent signup = new Intent(this, typeof(UserDetail));

                StartActivity(signup);

            };

            deposit.Click += delegate
            {
                Intent deposit = new Intent(this, typeof(depositMoney));

                StartActivity(deposit);

            };
            withdraw.Click += delegate
            {
                Intent withdraw = new Intent(this, typeof(withdrawMoney));

                StartActivity(withdraw);

            };
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}