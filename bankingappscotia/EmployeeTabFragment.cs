﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    public class EmployeeTabFragment : Fragment
    {
        EditText name;
        public EditText dob;
        public EditText email;
        public EditText password;
        public EditText contact;
        public EditText houseNo;
        public EditText city;
        public EditText postal;
        public EditText Question1;

        public Button continuebtn;
        public string isEmployee;

        //for creating alert
        Android.App.AlertDialog.Builder alert;


        DBHelperClass db;

        public override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this.Activity);
            base.OnCreate(savedInstanceState);

            // Create your fragment here

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View myView = inflater.Inflate(Resource.Layout.employeeSignUpLayout, container, false);


            name = myView.FindViewById<EditText>(Resource.Id.nameId);
            dob = myView.FindViewById<EditText>(Resource.Id.dob);
            email = myView.FindViewById<EditText>(Resource.Id.email);
            password = myView.FindViewById<EditText>(Resource.Id.Password);
            contact = myView.FindViewById<EditText>(Resource.Id.Contact);
            houseNo = myView.FindViewById<EditText>(Resource.Id.Address);
            city = myView.FindViewById<EditText>(Resource.Id.city);
            postal = myView.FindViewById<EditText>(Resource.Id.postalCode);
            Question1 = myView.FindViewById<EditText>(Resource.Id.Question1);


            continuebtn = myView.FindViewById<Button>(Resource.Id.continueBtn);


            // creating alert if user does not enter the value
            alert = new Android.App.AlertDialog.Builder(Context);



            //return base.OnCreateView(inflater, container, savedInstanceState);
            continuebtn.Click += delegate

            {
                string name_val = name.Text;
                string dob_val = dob.Text;
                string email_val = email.Text;
                string password_val = password.Text;
                string contact_val = contact.Text;
                string address_val = houseNo.Text;
                string city_val = city.Text;
                string postal_val = postal.Text;

                string Question1_val = Question1.Text;



                if (name_val.Trim().Equals("") || name_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter Your Name");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                else if (dob_val.Trim().Equals("") || dob_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Fill your DOB");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                else if (email_val.Trim().Equals("") || email_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Fill your Email Address");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (password_val.Trim().Equals("") || password_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Set Your Password");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (contact_val.Trim().Equals("") || contact_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Provide Your Phone Number");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (address_val.Trim().Equals("") || address_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Enter your address");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (city_val.Trim().Equals("") || city_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Fill the name of your city");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (postal_val.Trim().Equals("") || postal_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter your postal code");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (Question1_val.Trim().Equals("") || Question1_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter Account Number");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }

                else
                {

                    isEmployee = "true";

                    bool dataExists = false;

                    DateTime createDate = DateTime.Today;
                    string format = "yyyy-MM-dd";

                    IList emailIds = db.geUserEmailIds();
                    IList contacts = db.geUserContacts();
                    IList employeeIdList = db.geEmployeeIds();

                    int employeeId = 0;

                    SendEmails sendEmails = new SendEmails();

                    if (emailIds != null && emailIds.Count > 0 && contacts != null && contacts.Count > 0 &&
                    employeeIdList != null && employeeIdList.Count > 0)
                    {
                        if (emailIds.Contains(email_val) || contacts.Contains(contact_val))
                        {
                            dataExists = true;
                        }
                        else
                        {
                            employeeId = this.generateRandomEmployeeId();
                            for (int j = 0; j <= employeeIdList.Count - 1; j++)
                            {
                                if (employeeIdList.Contains(employeeId))
                                {
                                    employeeId = this.generateRandomEmployeeId();
                                    continue;
                                }
                                else
                                {
                                    db.insertEmployeeSignUpData(name_val, dob_val, email_val, password_val, contact_val, address_val,
                                    city_val, postal_val, Question1_val, employeeId, createDate.ToString(format), isEmployee);

                                    sendEmails.sendEmployeeSignUpEmail(name_val, email_val, password_val, contact_val, Question1_val, employeeId);

                                    break;
                                }
                            }

                        }
                    }
                    else
                    {
                        employeeId = this.generateRandomEmployeeId();

                        db.insertEmployeeSignUpData(name_val, dob_val, email_val, password_val, contact_val, address_val,
                                    city_val, postal_val, Question1_val, employeeId, createDate.ToString(format), isEmployee);


                        sendEmails.sendEmployeeSignUpEmail(name_val, email_val, password_val, contact_val, Question1_val, employeeId);

                        Console.WriteLine("New Employee registered successfully");

                    }
                    if (dataExists)
                    {
                        // Need to send a alert on signup scr saying email or contact number is already exist in system
                        Console.WriteLine("9999999999999999999999999999999999999999   email or contact number is already exist in system");
                    }



                    var loginPage = new Intent(Activity, typeof(MainActivity));
                   
                    StartActivity(loginPage);
                }


            };


            return myView;

        }

        private int generateRandomEmployeeId()
        {

            Random rnd = new Random();
            int empId = rnd.Next(100000, 999999);

            return empId;
        }

        public void alertOK(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("OK Button Pressed");
        }
        public void alertcancel(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("Cancel Button Pressed");
        }


    }
}