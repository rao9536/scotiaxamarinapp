﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "EmployeeClientInfo")]
    class EmployeeClientInfo : Activity
    {
        TextView AccountTxt;
        TextView nameTxt;
        TextView emailidTxt;
        TextView dobTxt;
        TextView balanceTxt;
        DBHelperClass myDB;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            myDB = new DBHelperClass(this);

            // Create your application here
            SetContentView(Resource.Layout.EmployeeClientInfo);
            string dbAccount = Intent.GetStringExtra("accountNumber");
            string dbName = Intent.GetStringExtra("name");
            string dbEmail = Intent.GetStringExtra("email");
            string dbDob = Intent.GetStringExtra("dob");
            string dbBalance = Intent.GetStringExtra("balance");

            AccountTxt = FindViewById<TextView>(Resource.Id.getAccount);
            nameTxt = FindViewById<TextView>(Resource.Id.getName);
            emailidTxt = FindViewById<TextView>(Resource.Id.getEmail);
            dobTxt = FindViewById<TextView>(Resource.Id.getDob);
            balanceTxt = FindViewById<TextView>(Resource.Id.getBalance);

            AccountTxt.Text = "Account Number " + dbAccount;
            nameTxt.Text = dbName;
            emailidTxt.Text = dbEmail;
            dobTxt.Text = dbDob;
            balanceTxt.Text = "Balance $ " + dbBalance;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}