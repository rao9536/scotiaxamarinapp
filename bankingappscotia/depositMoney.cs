﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "depositMoney")]
    public class depositMoney : Activity
    {
        EditText AccountNum;
        EditText amount;
        Button depositBtn;
        DBHelperClass myDB;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            myDB = new DBHelperClass(this);
            // Create your application here
            SetContentView(Resource.Layout.depositMoney);

            // set the values from layout by relating ids
            AccountNum = FindViewById<EditText>(Resource.Id.AccId);
            amount = FindViewById<EditText>(Resource.Id.deposit);
            depositBtn = FindViewById<Button>(Resource.Id.depositbtn);


            depositBtn.Click += delegate {
                string account = AccountNum.Text;
                int depositAmount = int.Parse(amount.Text);

                IList userObjList = myDB.getUserObjectByAccount(account);
                AccountObject moneyReceiver = (AccountObject)userObjList[0];

                int userAccBalance = int.Parse(moneyReceiver.GetInitialAcBalance());
                int userNewBalance = userAccBalance + depositAmount;

                myDB.updateInteracAccount(moneyReceiver.GetEmail(), userNewBalance);

                Intent goback = new Intent(this, typeof(EmployeeLoginOption));
                StartActivity(goback);
            };
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}