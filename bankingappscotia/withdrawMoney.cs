﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "withdrawMoney")]
    public class withdrawMoney : Activity
    {
        EditText AccountNum;
        EditText amount;
        Button withdrawBtn;
        DBHelperClass myDB;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            myDB = new DBHelperClass(this);
            // Create your application here
            SetContentView(Resource.Layout.withdrawMoney);

            // set the values from layout by relating ids
            AccountNum = FindViewById<EditText>(Resource.Id.AccId);
            amount = FindViewById<EditText>(Resource.Id.withdraw);
            withdrawBtn = FindViewById<Button>(Resource.Id.withdrawbtn);


            withdrawBtn.Click += delegate {

                string account = AccountNum.Text;
                int withdrawAmount = int.Parse(amount.Text);

                IList userObjList = myDB.getUserObjectByAccount(account);
                AccountObject moneyWithdrawer = (AccountObject)userObjList[0];

                int userAccBalance = int.Parse(moneyWithdrawer.GetInitialAcBalance());
                int userNewBalance = userAccBalance - withdrawAmount;

                myDB.updateInteracAccount(moneyWithdrawer.GetEmail(), userNewBalance);

                Intent goback = new Intent(this, typeof(EmployeeLoginOption));
                StartActivity(goback);
            };
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}