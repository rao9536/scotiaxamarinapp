﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    [Activity(Label = "ListPayee")]
    public class ListPayee : Activity
    {
        ListView myList;
        SearchView mySearch;
        ArrayAdapter myAdapter;
        DBHelperClass myDB;
        IList payeeList = new ArrayList();
        List<string> payees = new List<string>();
        List<InteracObject> list = new List<InteracObject>();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ListPayee);
            // Create your application here

            myDB = new DBHelperClass(this);

            //SetContentView(Resource.Layout.EmployeeFirstPage);
            myList = FindViewById<ListView>(Resource.Id.PayeeList);
            payeeList = myDB.gePayeeList();

            for (int i = 0; i <= payeeList.Count - 1; i++)
            {
                InteracObject interacObject = (InteracObject)payeeList[i];
                payees.Add(interacObject.GetName());
               
                list.Add((InteracObject)payeeList[i]);
            }

            PayeeCustomAdapter myAdapter = new PayeeCustomAdapter(this, list);
            myList.Adapter = myAdapter;
            myList.ItemClick += MyList_ItemClick;

            // get resource for search

            mySearch = FindViewById<SearchView>(Resource.Id.searchId);
            mySearch.QueryTextChange += MySearch_QueryTextChange;
        }

        private void MyList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var index = e.Position;
            // string myVal = myUserList[index];
            /*string[] dbData = myDB.selectByName(myVal.ToString());

            System.Console.WriteLine("Name from db is : " + dbData[0]);
            System.Console.WriteLine("email from db is : " + dbData[1]);
            System.Console.WriteLine("password from db is :  " + dbData[2]);
            System.Console.WriteLine("age from db is :  " + dbData[3]);*/

            // System.Console.WriteLine("I love to play  " + myVal);
            /* Intent ContactDetail = new Intent(this, typeof(ContactInfo));

             ContactDetail.PutExtra("name", dbData[0]);
             ContactDetail.PutExtra("email", dbData[1]);
             ContactDetail.PutExtra("password", dbData[2]);
             ContactDetail.PutExtra("age", dbData[3]);

             StartActivity(ContactDetail);*/

        }


        private void MySearch_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            var mySearchValue = e.NewText;
            System.Console.WriteLine("My search value is " + mySearchValue);
            string[] myArray = payees.ToArray();
            List<InteracObject> result = new List<InteracObject>();
            int position = Array.IndexOf(myArray, mySearchValue);
            System.Console.WriteLine("My search value position is " + position);
            if (position > -1)
            {
                string value = myArray[position];

                for (int i = 0; i <= payeeList.Count - 1; i++)
                {
                    InteracObject interacObject = (InteracObject)payeeList[i];
                    if (value.Equals(interacObject.GetName()))
                    {
                        result.Add(interacObject);
                    }
                }

                PayeeCustomAdapter myAdapter = new PayeeCustomAdapter(this, result);

                myList.Adapter = myAdapter;
                //myList.ItemClick += MyList_ItemClick;
            }
            else
            {
                PayeeCustomAdapter myAdapter = new PayeeCustomAdapter(this, list);
                myList.Adapter = myAdapter;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}