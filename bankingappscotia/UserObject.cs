﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace bankingappscotia
{
    class UserObject
    {
        public string accountNumber;
        public string name;
        public string email;
        public string contactNumber;
        public string houseNumber;
        public string city;
        public string seccurityValue;
        public string postalCode;
        public string dob;

        public  void SetAccountNumber(string acc)
        {
            accountNumber = acc;
        }

        public  string GetAccountNumber()
        {
            return accountNumber;
        }
        public void SetName(string str)
        {
            name = str;
        }
        public string GetName()
        {
            return name;
        }

        public void SetEmail(string str)
        {
            email = str;
        }

        public string GetEmail()
        {
            return email;
        }

        public void SetContactNumber(string str)
        {
            contactNumber = str;
        }

        public string GetContactNumber()
        {
            return contactNumber;
        }

        public void SetHouseNumber(string str)
        {
            houseNumber = str;
        }

        public string GetHouseNumber()
        {
            return houseNumber;
        }

        public void SetCity(string str)
        {
            city = str;
        }

        public string GetCity()
        {
            return city;
        }

        public void SetSeccurityValue(string str)
        {
            seccurityValue = str;
        }

        public string GetSeccurityValue()
        {
            return seccurityValue;
        }

        public void SetPostalCode(string str)
        {
            postalCode = str;
        }

        public string GetPostalCode()
        {
            return postalCode;
        }

        public void SetDOB(string str)
        {
            dob = str;
        }

        public string GetDOB()
        {
            return dob;
        }

    }
}