﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MimeKit;

namespace bankingappscotia
{
    class SendEmails
    {
        public static string fromEmail = "sugardaddy647@gmail.com";
        public static string fromEmailPwd = "Ramesh@98s";

        public void sendEmail(string name_val, string email_val, string password_val, double contact_val,string Question_val,int accountNumber, int initial_Balance,
            int minimum_Balance)
        {
            string body = "";

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Scotia Bank Account Operations Department", fromEmail));
            message.To.Add(new MailboxAddress(name_val, email_val));
            message.Subject = "Scotia Bank New Account Details";

            var bodyBuilder = new BodyBuilder();
            body = "Hi " + name_val + ",\n" +
                "Your Account is opened successfully. Please refer to below details to login your account \n" +
                "Account number : " + accountNumber +"\n"+
                "Password : " + password_val + "\n" +
               "Contact number : " + contact_val + "\n" +
               "Account Opening Balance : " + initial_Balance + "\n" +
               "Maintain Minimum Balance : " + minimum_Balance + "\n" +
               "Security Question Answer : " + Question_val;
                bodyBuilder.HtmlBody = body;
          

            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, false);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(fromEmail, fromEmailPwd);

                client.Send(message);
                client.Disconnect(true);
            }

        }

        public void balanceInquiry(string email, string balance, string acc)
        {

            string body = "";

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Scotia Bank Account Operations Department", fromEmail));
            message.To.Add(new MailboxAddress(email));
            message.Subject = "Scotia Bank Cheque Book Request Accepted";

            var bodyBuilder = new BodyBuilder();
            body = "Hi \n" +
                "Your account number  : " + acc + "\n" +
                "has  $: " + balance + "\n";
            bodyBuilder.HtmlBody = body;


            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, false);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(fromEmail, fromEmailPwd);

                client.Send(message);
                client.Disconnect(true);
            }

        }

        public void sendEmployeeSignUpEmail(string name_val, string email_val, string password_val, string contact_val, string Question_val, int employeeId)
        {
            string body = "";

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Scotia Bank Admin Department", fromEmail));
            message.To.Add(new MailboxAddress(name_val, email_val));
            message.Subject = "Scotia Bank Employee Registration Details";

            var bodyBuilder = new BodyBuilder();
            body = "Hi " + name_val + ",\n" +
                "Welcome to on board of Scotia Bank .Your are registered successfully. Please refer to below details to login your account \n" +
                "Employee Id : " + employeeId + "\n" +
                "Password : " + password_val + "\n" +
               "Contact number : " + contact_val + "\n" +
               "Security Question Answer : " + Question_val;
            bodyBuilder.HtmlBody = body;


            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, false);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(fromEmail, fromEmailPwd);

                client.Send(message);
                client.Disconnect(true);
            }

        }



        public void sendChangePasswordEmail(string newPwd, string currentPwd, string name, string toEmail)
        {

            string body = "";

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Scotia Bank Account Operations Department", fromEmail));
            message.To.Add(new MailboxAddress(name, toEmail));
            message.Subject = "Scotia Bank Account Password Changed";

            var bodyBuilder = new BodyBuilder();
            body = "Hi \n" +
                "Your password is changed successfully \n" +
                "You have change your password from : " + currentPwd + "\n" +
                "to new Password : " + newPwd + "\n";
            bodyBuilder.HtmlBody = body;


            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, false);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(fromEmail, fromEmailPwd);

                client.Send(message);
                client.Disconnect(true);
            }

        }

        public void updateCustomerAccountStatus(string email, int receiverNewBalance, bool sender)
        {
            string body = "";

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Scotia Bank Account Operations Department", fromEmail));
            message.To.Add(new MailboxAddress(email));
            message.Subject = "Scotia Bank Interac Request Update";

            var bodyBuilder = new BodyBuilder();
            if (sender)
            {
                body = "Hi \n" +
                    "You sent money successfully \n" +
                    "Your new balance is  : " + receiverNewBalance;
            }
            else
            {
                body = "Hi \n" +
                    "You received money successfully \n" +
                    "Your new balance is  : " + receiverNewBalance;
            }
            bodyBuilder.HtmlBody = body;


            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, false);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(fromEmail, fromEmailPwd);

                client.Send(message);
                client.Disconnect(true);
            }

        }

    

        public void sendChequeBookEmail(string name, string toEmail, string houseNUmber, string city, string postalCode)
        {
            string body = "";

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Scotia Bank Account Operations Department", fromEmail));
            message.To.Add(new MailboxAddress(name, toEmail));
            message.Subject = "Scotia Bank Cheque Book Request Accepted";

            var bodyBuilder = new BodyBuilder();
            body = "Hi "+ name + "\n" +
                "Your request for cheque book is accepted and you will get it in 15 days at  \n" +
                "house number  : " + houseNUmber + "\n" +
                "City : " + city + "\n"+
                "Postal Code :"+ postalCode;
            bodyBuilder.HtmlBody = body;


            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect("smtp.gmail.com", 587, false);

                // Note: only needed if the SMTP server requires authentication
                client.Authenticate(fromEmail, fromEmailPwd);

                client.Send(message);
                client.Disconnect(true);
            }

        }
    }
}