﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{

    [Activity(Label = "AddPayee")]
    public class AddPayee : Activity
    {
        DBHelperClass db;
        EditText name;
        EditText email;
        EditText securitQuesValue;
        Button saveBtn;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddPayee);
            // Create your application here

            name = FindViewById<EditText>(Resource.Id.name);
            email = FindViewById<EditText>(Resource.Id.email);
            securitQuesValue = FindViewById<EditText>(Resource.Id.securityQuestionValue);
            saveBtn = FindViewById<Button>(Resource.Id.addBtn);

            saveBtn.Click += Submit_Click;
        }

        private void Submit_Click(object sender, EventArgs e)
        {

            string accountNum = GlobalVariable.GetAccount();
            DateTime createDate = DateTime.Today;
            string format = "yyyy-MM-dd";

            db.saveInterac(name.Text,email.Text, securitQuesValue.Text, createDate.ToString(format));

            Intent eTransfer = new Intent(this, typeof(eTransfer));
            StartActivity(eTransfer);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}