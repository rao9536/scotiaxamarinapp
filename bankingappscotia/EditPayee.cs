﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{

    [Activity(Label = "EditPayee")]
    public class EditPayee : Activity
    {
        DBHelperClass db;
        EditText email;
        EditText name;
        EditText secQue;
        Button EditBtn;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            db = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EditPayee);

            email = FindViewById<EditText>(Resource.Id.email);
            name = FindViewById<EditText>(Resource.Id.name);
            secQue = FindViewById<EditText>(Resource.Id.secQues);
            EditBtn = FindViewById<Button>(Resource.Id.EditBtn);

            string[] interacObj = db.getInteracObj(GlobalVariable.GetEditPayeeEmail().ToString());

            name.Text = interacObj[0];
            email.Text = interacObj[1];
            secQue.Text = interacObj[2];

            email.Enabled = false;
            name.Enabled = false;
            secQue.Enabled = false;

            EditBtn.Click += Edit_Click;
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            email.Enabled = false;
            name.Enabled = true;
            secQue.Enabled = true;

            EditBtn.Text = "Save";
            EditBtn.Click += Save_Click;

        }

        private void Save_Click(object sender, EventArgs e)
        {
            email = FindViewById<EditText>(Resource.Id.email);
            name = FindViewById<EditText>(Resource.Id.name);
            secQue = FindViewById<EditText>(Resource.Id.secQues);

            db.updateInteracPayeeDetails(email.Text, name.Text, secQue.Text);

            Intent eTransfer = new Intent(this, typeof(eTransfer));
            StartActivity(eTransfer);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            // set the menu layout on Main Activity  
            MenuInflater.Inflate(Resource.Menu.mymenu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.logout:
                    {
                        // add your code 
                        Intent logOut = new Intent(this, typeof(MainActivity));
                        StartActivity(logOut);
                        return true;
                    }
                case Resource.Id.chpwd:
                    {
                        // add your code 
                        Intent chnagePassword = new Intent(this, typeof(ChangePassword));
                        StartActivity(chnagePassword);
                        return true;
                    }
                case Resource.Id.about:
                    {
                        Intent aboutUs = new Intent(this, typeof(AboutUs));
                        StartActivity(aboutUs);
                        return true;
                    }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}