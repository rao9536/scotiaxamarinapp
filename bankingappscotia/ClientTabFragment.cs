﻿using System;
using System.Collections;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace bankingappscotia
{
    public class ClientTabFragment : Fragment
    {
        EditText name;
       public EditText dob;
       public EditText email;
       public EditText password;
       public EditText contact;
       public EditText houseNo;
       public EditText initialBalance;
       public EditText minimumBalance;
       public EditText city;
       public EditText postal;
       public EditText Question;
    
       public Button continuebtn;

        //for creating alert
        Android.App.AlertDialog.Builder alert;
        DBHelperClass dBHelper;

        public override void OnCreate(Bundle savedInstanceState)
        {

           dBHelper = new DBHelperClass(this.Activity);
            base.OnCreate(savedInstanceState);

            // Create your fragment here

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View myView = inflater.Inflate(Resource.Layout.ClientSignUpLayout,container, false);

            name = myView.FindViewById<EditText>(Resource.Id.nameId);
            dob = myView.FindViewById<EditText>(Resource.Id.dob);
            email = myView.FindViewById<EditText>(Resource.Id.email);
            password = myView.FindViewById<EditText>(Resource.Id.Password);
            contact = myView.FindViewById<EditText>(Resource.Id.Contact);
            houseNo = myView.FindViewById<EditText>(Resource.Id.Address);
            initialBalance = myView.FindViewById<EditText>(Resource.Id.initialBalance);
            minimumBalance = myView.FindViewById<EditText>(Resource.Id.minimumBalance);
            city = myView.FindViewById<EditText>(Resource.Id.city);
            postal = myView.FindViewById<EditText>(Resource.Id.postalCode);
            Question = myView.FindViewById<EditText>(Resource.Id.Question1);
           

            continuebtn = myView.FindViewById<Button>(Resource.Id.continueBtn);


            // creating alert if user does not enter the value
            alert = new Android.App.AlertDialog.Builder(Context);


            //return base.OnCreateView(inflater, container, savedInstanceState);
            continuebtn.Click += delegate

            {
                string name_val = name.Text;
                string dob_val = dob.Text;
                string email_val = email.Text;
                string password_val = password.Text;
                double contact_val = Convert.ToDouble(contact.Text);
                int houseNo_val = Convert.ToInt32(houseNo.Text);
                int initial_Balance = Convert.ToInt32(initialBalance.Text);
                int minimum_Balance = Convert.ToInt32(minimumBalance.Text);
                string city_val = city.Text;
                string postal_val = postal.Text;
                string Question_val = Question.Text;
           


                if (name_val.Trim().Equals("") || name_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter Your Name");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                else if (dob_val.Trim().Equals("") || dob_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Fill your DOB");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                else if (email_val.Trim().Equals("") || email_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Fill your Email Address");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (password_val.Trim().Equals("") || password_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Set Your Password");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (contact_val.Equals(" "))
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Provide Your Phone Number");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (houseNo_val.Equals(""))
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Enter your house number");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (city_val.Trim().Equals("") || city_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Fill the name of your city");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (postal_val.Trim().Equals("") || postal_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter your postal code");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (Question_val.Trim().Equals("") || Question_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Please Enter Account Number");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();
                }
                else if (initial_Balance.Equals(""))
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Enter initial account balance");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }
                else if (minimum_Balance.Equals(""))
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("Enter minimum balance to maintain");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }

                else
                {
                    string isEmployee = "false";
                    bool dataExists = false;

                    DateTime createDate = DateTime.Today;
                    string format = "yyyy-MM-dd";

                    IList emailIds = dBHelper.geUserEmailIds();
                    IList contacts = dBHelper.geUserContacts();
                    IList accountNos = dBHelper.geUseraccountNos();

                    int accountNumber = 0;

                    SendEmails sendEmails = new SendEmails();

                    if (emailIds != null && emailIds.Count > 0 && contacts != null && contacts.Count > 0 &&
                    accountNos != null && accountNos.Count > 0)
                    {
                        if (emailIds.Contains(email_val) || contacts.Contains(contact_val))
                        {
                            dataExists = true;
                        }else
                        {
                            accountNumber = this.generateRandomAccounNumber();
                            for(int j=0; j<= accountNos.Count - 1; j++)
                            {
                                if (accountNos.Contains(accountNumber))
                                {
                                    accountNumber = this.generateRandomAccounNumber();
                                    continue;
                                }
                                else
                                {
                                    dBHelper.insertSignUpData(name_val, dob_val, email_val, password_val, contact_val, houseNo_val,
                                    city_val, postal_val, Question_val, accountNumber, createDate.ToString(format), isEmployee);

                                    dBHelper.insertAccountData(email_val,Question_val, accountNumber, createDate.ToString(format), 
                                        initial_Balance, minimum_Balance);

                                    sendEmails.sendEmail(name_val, email_val, password_val, contact_val, Question_val, accountNumber, initial_Balance, minimum_Balance);

                                    break;
                                }
                            }

                        }
                    }else
                    {
                        accountNumber = this.generateRandomAccounNumber();

                        dBHelper.insertSignUpData(name_val, dob_val, email_val, password_val, contact_val, houseNo_val,
                            city_val, postal_val, Question_val, accountNumber, createDate.ToString(format), isEmployee);

                        dBHelper.insertAccountData(email_val, Question_val, accountNumber, createDate.ToString(format),
                                        initial_Balance, minimum_Balance);

                        sendEmails.sendEmail(name_val, email_val, password_val, contact_val, Question_val, accountNumber, initial_Balance, minimum_Balance);

                        Console.WriteLine("New User registered successfully");

                    }

                    if (dataExists)
                    {
                        // Need to send a alert on signup scr saying email or contact number is already exist in system
                        Console.WriteLine("9999999999999999999999999999999999999999   email or contact number is already exist in system");
                    }


                    var loginPage = new Intent(Activity, typeof(MainActivity));
                    // intent.PutExtra("current_play_id", playId);
                    StartActivity(loginPage);
                }


            };


            return myView;
       
        }

        private int generateRandomAccounNumber()
        {

            Random rnd = new Random();
            int accNum = rnd.Next(10000000, 99999999);

            return accNum;
        }

        public void alertOK(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("OK Button Pressed");
        }
        public void alertcancel(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("Cancel Button Pressed");
        }


    }
}